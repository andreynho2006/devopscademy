#! /bin/bash

# available and user memory
echo "-----AVAILABLE AND USED MEMORY-----"
echo "$(cat /proc/meminfo)"
echo ""

# amount of physical  and swap memory
echo "-----AMOUNT PHYSICAL AND SWAP MEMORY-----"
echo "$(free)"
echo ""

# virtual memory statistic
echo "-----ViRtuAL MEMORY STATISTIC-----"
echo "$(vmstat)"

